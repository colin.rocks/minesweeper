#include "TextureMagic.h"

unordered_map<string, sf::Texture> TextureMagic::textures;

void TextureMagic::LoadTexture(string fileName)
{
	string path = "images/";
	path += fileName + ".png";

	textures[fileName].loadFromFile(path);
}

sf::Texture& TextureMagic::GetTexture(string name)
{
	if (textures.find(name) == textures.end())
		LoadTexture(name);
	return textures[name];
}

void TextureMagic::ClearTextures()
{
	textures.clear();
}