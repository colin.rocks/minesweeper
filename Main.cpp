#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include "TextureMagic.h"
#include "Board.h"

using namespace std;

bool inRange(sf::Vector2f vector, int x, int y)
{
    return (x - vector.x <= 64 && x - vector.x >= 0) &&
        (y - vector.y <= 64 && y - vector.y >= 0);
}

int main()
{
    fstream cfg("boards/config.cfg");
    int width, height, mines;
    cfg >> width;
    cfg >> height;
    cfg >> mines;
    
    Board::GenerateBoard(width, height, mines);
    Board::LoadBoard("board");
    
    sf::RenderWindow window(sf::VideoMode(width*32, height*32+88), "Minesweeper");

    sf::Sprite smiley(TextureMagic::GetTexture("face_happy"));
    smiley.setPosition(sf::Vector2f(width * 32 / 2 - 32, height * 32));
    sf::Sprite debug(TextureMagic::GetTexture("debug"));
    debug.setPosition(sf::Vector2f(width * 32 / 2 + 4 * 32 - 32, height * 32));
    sf::Sprite load1(TextureMagic::GetTexture("test_1"));
    load1.setPosition(sf::Vector2f(width * 32 / 2 + 6 * 32 - 32, height * 32));
    sf::Sprite load2(TextureMagic::GetTexture("test_2"));
    load2.setPosition(sf::Vector2f(width * 32 / 2 + 8 * 32 - 32, height * 32));
    sf::Sprite load3(TextureMagic::GetTexture("test_3"));
    load3.setPosition(sf::Vector2f(width * 32 / 2 + 10 * 32 - 32, height * 32));

    sf::Sprite sign(TextureMagic::GetTexture("digits"), sf::IntRect(21 * 10, 0, 21, 32));
    sign.setPosition(sf::Vector2f(0, height * 32));
    sf::Sprite number1(TextureMagic::GetTexture("digits"));
    number1.setPosition(sf::Vector2f(21, height * 32));
    sf::Sprite number2(TextureMagic::GetTexture("digits"));
    number2.setPosition(sf::Vector2f(42, height * 32));
    sf::Sprite number3(TextureMagic::GetTexture("digits"));
    number3.setPosition(sf::Vector2f(63, height * 32));

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    std::cout << "the left button was pressed" << std::endl;
                    std::cout << "mouse x: " << event.mouseButton.x << std::endl;
                    std::cout << "mouse y: " << event.mouseButton.y << std::endl;
                    Board::Clicked(event.mouseButton.x, event.mouseButton.y, false);
                    if (inRange(smiley.getPosition(), event.mouseButton.x, event.mouseButton.y))
                    { 
                        Board::GenerateBoard(width, height, mines);
                        Board::LoadBoard("board");
                    }
                    if (inRange(debug.getPosition(), event.mouseButton.x, event.mouseButton.y))
                        Board::SetDebug();
                    if (inRange(load1.getPosition(), event.mouseButton.x, event.mouseButton.y))
                        Board::LoadBoard("testboard1");
                    if (inRange(load2.getPosition(), event.mouseButton.x, event.mouseButton.y))
                        Board::LoadBoard("testboard2");
                    if (inRange(load3.getPosition(), event.mouseButton.x, event.mouseButton.y))
                        Board::LoadBoard("testboard3");
                }
                if (event.mouseButton.button == sf::Mouse::Right)
                {
                    std::cout << "the right button was pressed" << std::endl;
                    std::cout << "mouse x: " << event.mouseButton.x << std::endl;
                    std::cout << "mouse y: " << event.mouseButton.y << std::endl;
                    Board::Clicked(event.mouseButton.x, event.mouseButton.y, true);
                }
            }
        }
        window.clear();
        if (Board::GetWinstate() == 0)
            smiley.setTexture(TextureMagic::GetTexture("face_happy"));
        if (Board::GetWinstate() == 1)
            smiley.setTexture(TextureMagic::GetTexture("face_win"));
        else if (Board::GetWinstate() == 2)
            smiley.setTexture(TextureMagic::GetTexture("face_lose"));

        Board::UpdateBoard(window);
        window.draw(smiley);
        window.draw(debug);
        window.draw(load1);
        window.draw(load2);
        window.draw(load3);
        if (Board::bombCount < 0)
        {
            window.draw(sign);
            number1.setTextureRect(sf::IntRect(-1 * 21 * (Board::bombCount / 100), 0, 21, 32));
            number2.setTextureRect(sf::IntRect(-1 * 21 * ((Board::bombCount / 10) % 10), 0, 21, 32));
            number3.setTextureRect(sf::IntRect(-1 * 21 * (Board::bombCount % 10), 0, 21, 32));
        }
        else
        {
            number1.setTextureRect(sf::IntRect(21 * (Board::bombCount / 100), 0, 21, 32));
            number2.setTextureRect(sf::IntRect(21 * ((Board::bombCount / 10) % 10), 0, 21, 32));
            number3.setTextureRect(sf::IntRect(21 * (Board::bombCount % 10), 0, 21, 32));
        }
        window.draw(number1);
        window.draw(number2);
        window.draw(number3);
        window.display();
    }
    TextureMagic::ClearTextures();
    return 0;
}