#include "Board.h"

using namespace std;

vector<Tile> Board::tiles;
bool Board::debugState;
int Board::bombCount = 0, Board::width = 0, Board::height = 0, Board::totalMines = 0;
unsigned int Board::winState = 0;

void Board::GenerateBoard(int rows, int cols, int mines)
{
	width = rows;
	height = cols;
	totalMines = mines;
	fstream file("boards/board.brd", ios::in | ios::out | ofstream::trunc);
	for (int i = 0; i < cols; i++)
	{
		for (int i = 0; i < rows; i++)
			file << 0; //mines times: random fileio...if 0, change to 1, if 1, repeat random fileio until not 1
		file << "\n";
	}
	int i = 0;
	while (i != totalMines)
	{
		file.clear();
		char ch;
		file.seekg(Random::Int(0, (rows * cols) + 2*cols-1), ios::beg);
		file.get(ch);
		file.seekg(-1, ios::cur);
		if (ch == '\n' || ch == '1')
			;
		else
		{
			file << 1;
			i++;
		}
	}
}

void Board::LoadBoard(string fileName)
{
	tiles.clear();
	bombCount = 0;
	winState = 0;
	ifstream file("boards/" + fileName + ".brd");
	char ch;
	int i = 0;
	int j = 0;
	while (file.get(ch))
	{
		Tile tile;
		if (ch == '\n')
		{
			i = -1;
			j++;
		}
		else if (ch == '1')
		{
			bombCount++;
			tile.isMine = true;
			tile.xpos = i * 32;
			tile.ypos = j * 32;
			tiles.push_back(tile);
		}
		else
		{
			tile.isMine = false;
			tile.xpos = i * 32;
			tile.ypos = j * 32;
			tiles.push_back(tile);
		}
		i++;
	}
	totalMines = bombCount;
	for (unsigned int i = 0; i < tiles.size(); i++)
		tiles[i].adjacentCount = CountAdjacents(tiles[i]);
}

void Board::UpdateBoard(sf::RenderWindow& window)
{	
	for (unsigned int i = 0; i < tiles.size(); i++)
	{
		tiles[i].UpdateTile();
		window.draw(tiles[i].background);
		window.draw(tiles[i].sprite);
	}
	vector<sf::Sprite> debugSprites = Debug();
	if (debugState && winState != 2 && winState != 1)
		for (unsigned int i = 0; i < debugSprites.size(); i++)
			window.draw(debugSprites[i]);
	CheckWin();
	if (winState == 2)
	{
		for (unsigned int i = 0; i < tiles.size(); i++)
			if (!tiles[i].isRevealed)
			{
				for (unsigned int i = 0; i < debugSprites.size(); i++)
					window.draw(debugSprites[i]);
				for (unsigned int i = 0; i < tiles.size(); i++)
					if (tiles[i].isMine && !tiles[i].isFlag)
						tiles[i].gg = true;
			}
	}
	if (winState == 3)
		for (unsigned int i = 0; i < tiles.size(); i++)
			if (!tiles[i].isRevealed)
			{
				tiles[i].isFlag = true;
				bombCount--;
				winState = 1;
			}
}

void Board::Clicked(int x, int y, bool clickType)
{
	x /= 32;
	y /= 32;

	if (!clickType && winState != 2)
	{
		for (unsigned int i = 0; i < tiles.size(); i++)
		{
			if (x == ((tiles[i].xpos / 32)) && y == ((tiles[i].ypos / 32)) && !(tiles[i].isFlag))
			{
				if (tiles[i].isMine)
					winState = 2;
				tiles[i].isRevealed = true;
				RecursiveClear(tiles[i]);
			}
		}
	}
	else if (clickType && winState != 1 && winState != 2)
	{
		for (unsigned int i = 0; i < tiles.size(); i++)
			if (x == ((tiles[i].xpos / 32)) && y == ((tiles[i].ypos / 32)) && !tiles[i].isRevealed)
			{
				tiles[i].isFlag ^= true;
				if (tiles[i].isFlag)
					bombCount--;
				else
					bombCount++;
			}
	}
}

int Board::CountAdjacents(Tile &tile)
{
	int mineCount = 0;
	for (unsigned int i = 0; i < tiles.size(); i++)
	{
		if (tiles[i].xpos == tile.xpos - 32 && tiles[i].ypos == tile.ypos + 32)
			tile.adjacent.push_back(&tiles[i]);
		if (tiles[i].xpos == tile.xpos && tiles[i].ypos == tile.ypos + 32)
			tile.adjacent.push_back(&tiles[i]);
		if (tiles[i].xpos == tile.xpos + 32 && tiles[i].ypos == tile.ypos + 32)
			tile.adjacent.push_back(&tiles[i]);
		if (tiles[i].xpos == tile.xpos - 32 && tiles[i].ypos == tile.ypos)
			tile.adjacent.push_back(&tiles[i]);
		if (tiles[i].xpos == tile.xpos + 32 && tiles[i].ypos == tile.ypos)
			tile.adjacent.push_back(&tiles[i]);
		if (tiles[i].xpos == tile.xpos - 32 && tiles[i].ypos == tile.ypos - 32)
			tile.adjacent.push_back(&tiles[i]);
		if (tiles[i].xpos == tile.xpos && tiles[i].ypos == tile.ypos - 32)
			tile.adjacent.push_back(&tiles[i]);
		if (tiles[i].xpos == tile.xpos + 32 && tiles[i].ypos == tile.ypos - 32)
			tile.adjacent.push_back(&tiles[i]);
	}
	for (unsigned int i = 0; i < tile.adjacent.size(); i++)
		if (tile.adjacent[i]->isMine)
				mineCount++;
	return mineCount;
}

vector<sf::Sprite> Board::Debug()
{
	sf::Sprite sprite;
	vector<sf::Sprite> sprites;
	if (winState == 0 || winState == 2)
	{
		for (unsigned int i = 0; i < tiles.size(); i++)
			if (tiles[i].isMine)
			{
				sprite.setTexture(TextureMagic::GetTexture("mine"));
				sprite.setPosition(sf::Vector2f(tiles[i].xpos, tiles[i].ypos));
				sprites.push_back(sprite);
			}
		return sprites;
	}
}

void Board::SetDebug()
{
	debugState ^= true;
}

unsigned int Board::GetWinstate()
{
	return winState;
}

void Board::CheckWin()
{
	if (winState == 0)
	{
		int revealCount = 0;
		for (unsigned int i = 0; i < tiles.size(); i++)
			if (tiles[i].isRevealed && !tiles[i].isMine)
				revealCount++;
		if (revealCount + totalMines == tiles.size())
			winState = 3;
	}
}

void Board::RecursiveClear(Tile &tile)
{
	if (tile.adjacentCount == 0 && !tile.isMine)
		for (unsigned int i = 0; i < tile.adjacent.size(); i++)
			if (!tile.adjacent[i]->isRevealed && !tile.adjacent[i]->isFlag && !tile.adjacent[i]->isMine)
			{
				tile.adjacent[i]->isRevealed = true;
				RecursiveClear(*tile.adjacent[i]);
			}
}