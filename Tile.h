#pragma once

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "TextureMagic.h"

using namespace std;

struct Tile
{
	int xpos, ypos, adjacentCount;
	bool isMine, isFlag, isRevealed, gg;
	vector<Tile*> adjacent;
	string texture;
	sf::Sprite sprite;
	sf::Sprite background;

	void UpdateTile();
	Tile();
};