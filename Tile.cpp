#include "Tile.h"

Tile::Tile()
{
	this->xpos = 0;
	this->ypos = 0;
	this->adjacentCount = 0;
	this->isMine = false;
	this->isFlag = false;
	this->isRevealed = false;
	this->gg = false;
	this->background.setTexture(TextureMagic::GetTexture("tile_revealed"));
}

void Tile::UpdateTile()
{
	if (this->gg && this->isMine)
		this->texture = "tile_revealed";
	else if (this->isRevealed && this->isMine)
		this->texture = "mine";
	else if (this->isFlag)
		this->texture = "flag";
	else if (!(this->isRevealed))
		this->texture = "tile_hidden";
	else if (this->isRevealed && this->isMine)
		this->texture = "mine";
	else if (this->isRevealed && (this->adjacentCount == 0))
		this->texture = "tile_revealed";
	else
		this->texture = "number_" + to_string(this->adjacentCount);
	this->sprite.setTexture(TextureMagic::GetTexture(texture));
	this->sprite.setPosition(sf::Vector2f(this->xpos, this->ypos));
	this->background.setPosition(sf::Vector2f(this->xpos, this->ypos));
}