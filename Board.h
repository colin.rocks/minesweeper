#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include "Tile.h"
#include "Random.h"



class Board
{
	static int width, height, totalMines;
	static vector<Tile> tiles;
	static bool debugState;
	static unsigned int winState;
	
public:
	static int bombCount;
	static void GenerateBoard(int rows, int cols, int mines);
	static void LoadBoard(string fileName);
	static void UpdateBoard(sf::RenderWindow& window);
	static void Clicked(int x, int y, bool clickType);
	static int CountAdjacents(Tile &tile);
	static vector<sf::Sprite> Debug();
	static void SetDebug();
	static unsigned int GetWinstate();
	static void CheckWin();
	static void RecursiveClear(Tile &tile);
};